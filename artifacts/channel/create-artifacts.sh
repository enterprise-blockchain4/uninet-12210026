# set PATH so it includes HLF bin if it exists
if [ -d "/workspaces/HLF-UNINETWORK_0/fabric-samples/bin" ] ; then
    PATH="/workspaces/HLF-UNINETWORK_0/fabric-samples/bin:$PATH"
fi

chmod -R 0755 ./crypto-config
# Delete existing artifacts
rm -rf ./crypto-config
rm genesis.block mychannel.tx
rm -rf ../../channel-artifacts/*

#Generate Crypto artifacts for organizations
cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/

# Set the path to the configtx.yaml file
export FABRIC_CFG_PATH=$PWD


# Generate the genesis block for the University Consortium Orderer
configtxgen -profile confectionaryOrdererGenesis -channelID ordererchannel -outputBlock confectionary-genesis.block

# Create the channel confectionaryChannel
configtxgen -outputCreateChannelTx ./confectionary-channel.tx -profile confectionaryChannel -channelID confectionarychannel

configtxgen -outputAnchorPeersUpdate Org1Anchors.tx -profile confectionaryChannel -channelID confectionarychannel -asOrg Org1

configtxgen -outputAnchorPeersUpdate Org2Anchors.tx -profile confectionaryChannel -channelID confectionarychannel -asOrg Org2

configtxgen -outputAnchorPeersUpdate Org3Anchors.tx -profile confectionaryChannel -channelID confectionarychannel -asOrg Org3